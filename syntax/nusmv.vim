" Language:    NuSMV
" Maintainer:  Wannes Meert <wannes.meert@cs.kuleuven.be>
" Last Change: 2010 07 18
" Url:         http://nusmv.irst.itc.it/

if version < 600
	syntax clear
elseif exists("b:current_syntax")
	finish
endif

" Case sensitive
syn case match

syn cluster	nusmvNotContained	contains=nusmvTodo

" Module
syn region	nusmvModule		start="^\s*MODULE " end="\s*MODULE" contains=ALLBUT,@nusmvNotContained
syn keyword     nusmvModuleKeyword      contained MODULE

" Identifiers
syn match	nusmvValue		'\(TRUE\|FALSE\)'
syn match	nusmvValue		'\d+'
syn match	nusmvUserType		'[a-zA-Z][a-zA-Z0-9_]*('he=e-1
syn match	nusmvUserType		'[a-zA-Z][a-zA-Z0-9_]*\s*$'

" Keywords
syn keyword	nusmvSectionKeyword	VAR ASSIGN FAIRNESS TRANS INIT DEFINE CONSTANTS IVAR INVAR SPEC CTLSPEC LTLSPEC PSLSPEC COMPUTE INVARSPEC JUSTICE COMPASSION ISA CONSTRAINT SIMPWFF CTLWFF LTLWFF PSLWFF COMPWFF
syn keyword 	nusmvStorageKeyword	boolean array of integer real word word1 bool
syn keyword 	nusmvControlKeyword	process self
syn keyword 	nusmvConditionalKeyword	case esac

" Operators
syn keyword 	nusmvOperator		init next EX AX EF AF EG AG E F O G H X Y Z A U S V T BU EBF ABF ABG
syn match	nusmvOperator		':'
syn match	nusmvOperator		'>'
syn match	nusmvOperator		'<'
syn match	nusmvOperator		'!'
syn match	nusmvOperator		'+'
syn match	nusmvOperator		'-'
syn match	nusmvOperator		'*'
syn match	nusmvOperator		'/'
syn match	nusmvOperator		'='
syn match	nusmvOperator		'|'
syn match	nusmvOperator		'&'
syn match	nusmvOperator		'\.'

syn match	nusmvDelimiter		','
syn match	nusmvDelimiter		';'

" Comments
syn match	nusmvComment		"--.*$" contains=nusmvTodo,nusmvCommentSection
syn keyword	nusmvTodo		contained TODO FIXME XXX
syn keyword	nusmvCommentSection	contained Summary Atoms Note

hi def link nusmvDelimiter		Delimiter
hi def link nusmvCommentSection		Underlined
hi def link nusmvValue			Number

hi def link nusmvModuleKeyword		Title
hi def link nusmvSectionKeyword		Tag
hi def link nusmvStorageKeyword		Special
hi def link nusmvControlKeyword		Special
hi def link nusmvConditionalKeyword	Conditional
hi def link nusmvUserType		Type

hi def link nusmvOperator		Function

hi def link nusmvComment		Comment
hi def link nusmvTodo			Todo

let b:current_syntax = "nusvm"

