# vim-Syntax-NuSMV
This project was forked from [wmnusmv](https://github.com/wannesm/wmnusmv.vim) and focusses
on improving the vim syntax file.

## Summary
Vim syntax file for [NuSMV](http://nusmv.irst.itc.it/).

## Installation
First download the `nusvm.vim` file in your `~/.vim/syntax` directory. Then add the following 
lines to your `~/.vimrc` file:

```
syntax on
filetype on
au BufNewFile,BufRead *.smv set filetype=nusmv
```

## Contributors
------------
Dennis Degryse [dennisdegryse@gmail.com]

